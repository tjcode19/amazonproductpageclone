import classes from './App.module.css';

import ProductPreview from './ProductPreview/ProductPreview';
import ProductDetails from './ProductDetails/ProductDetails';
import TopBar from './TopBar/TopBar';
import React, { Component } from 'react';
import ProductData from './utils/ProductData';

class App extends Component {

  state = {
    productData: ProductData,
    currentPreviewImagePos: 0,
    currentFeatureBtnPos: 0,
    showHeartBeat: false
  }

  onColorOptionClicked = (pos) => {
    this.setState({ currentPreviewImagePos: pos })
  }

  onFeatureBtnClicked = (pos) => {
    let updateShow = false;
    if(pos ==1){
      updateShow = true
    }
    this.setState({ currentFeatureBtnPos: pos, showHeartBeat:updateShow })
  }

  render() {
    return (
      <div className="App">
        <TopBar />

        <div className={classes.MainContainer}>
          <div className={classes.ProductPreview}>
            <ProductPreview data={this.state.productData}
              currentPreviewImage={this.state.productData.colorOptions[this.state.currentPreviewImagePos].imageUrl}
              showHeartBeat={this.state.showHeartBeat} />
          </div>

          <div className={classes.ProductDetails}>
            <ProductDetails data={this.state.productData}
              onColorOptionClicked={this.onColorOptionClicked}
              currentPreviewImagePos={this.state.currentPreviewImagePos} 
              onFeatureBtnClicked={this.onFeatureBtnClicked} 
              currentFeatureBtnPos={this.state.currentFeatureBtnPos}/>
          </div>
        </div>
      </div>
    );
  }

}

export default App;
