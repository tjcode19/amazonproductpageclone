import React from 'react'

import classes from './TopBar.module.css'

const TopBar = () => {
    return (
        <header>

            <nav className={classes.TopBar}>
                <img src="https://www.doorwaysva.org/wp-content/uploads/2019/06/amazon-logo.png" className="App-logo" alt="logo" />
            </nav>
        </header>
    )

}
export default TopBar