import React from 'react'

import classes from './ProductDetails.module.css'

const ProductDetails = (props) => {

    const colorType = props.data.colorOptions.map((item, pos) => {
        const classArr = [classes.ProductImage]
        if (pos === props.currentPreviewImagePos) {
            classArr.push(classes.SelectProductImage)
        }
        return (<img key={pos} className={classArr.join(' ')} src={item.imageUrl} alt={item.styleName} onClick={() => props.onColorOptionClicked(pos)} />)
    }
    );

    const featureBtn = props.data.featureList.map((item, pos) => {
        const classArr = [classes.FeatureItem]
        if (pos === props.currentFeatureBtnPos) {
            classArr.push(classes.SelectedFeatureItem)
        }
        return (<button onClick={() => props.onFeatureBtnClicked(pos)} key={pos} className={classArr.join(' ')} >{item}</button>)
    })


    return (

        <div className={classes.ProductDetails}>
            <h1 className={classes.ProductTitle}>{props.data.title}</h1>
            <p className={classes.ProductDescription}>{props.data.description}</p>

            <h3 className={classes.SectionHeading}>Select Colour</h3>
            <div>
                {colorType}
            </div>

            <h3 className={classes.SectionHeading}>Feature</h3>
            <div>
                {featureBtn}
            </div>
            <button className={classes.PrimaryButton}>Buy Now</button>
        </div>
    );
}

export default ProductDetails;